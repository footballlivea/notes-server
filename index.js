let express = require('express');
let app = express();
let pageController = require('./server_page');
let noteController = require('./server_note')

app.use('/script',express.static(__dirname+'/js'))
app.use('/style',express.static(__dirname+'/css'))


pageController(app,express);
noteController(app);

app.listen(3000)
