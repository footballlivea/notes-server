
const bodyParser = require("body-parser");

module.exports = (app)=>{
    let controller = new Users();
    app.post('/add',bodyParser.json(),controller.add.bind(controller))
    app.get('/show',bodyParser.json(),controller.show.bind(controller))
}


class Users {
    constructor(){
        this.notes = []
    }
    add(req,res){
        let note = req.body;
        this.notes.push(note)
        res.send(this.notes);
    }
    show(req,res){
        res.setHeader('Content-Type','application/json');
        res.send(this.notes)
    }
}