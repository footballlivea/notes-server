class NoteAdd{
    constructor(selector,conServ){
        this.selector = selector;
        this.noteServices = noteServices; 
        this.onAdd = ()=>{}
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.init();
                this.binds();
            }
        );
    }
    init(){
        this.conteiner = document.querySelector(this.selector);
        this.name = this.conteiner.querySelector('.note_name');
        this.text = this.conteiner.querySelector('.note_text');;
        this.addBtn = this.conteiner.querySelector('button');
    }
    binds(){
        this.addBtn.addEventListener('click',()=>this.add());
    }
    add(){
        if(this.name.value==''||this.text.value==''){
                alert('Заполните поля ввода')
            }else{
            let not = new Note(
            this.name.value,
            this.text.value
        );
        this.noteServices.addNote(not).then(r=>{
            // if(r.status === "error") this.addError(r.error);
            // else 
            this.addSuccess();
        })
    }
       
    }
    // addError(text){
    //     alert(text);
    // }
    addSuccess(){
        this.clearForm();
        this.onAdd();
    }
    clearForm(){
            this.name.value=''
            this.text.value=''
    }
}