class NoteServices{
    addNote(not){
        return fetch('/add',{
                method:'POST',
                headers:{
                    'Content-Type':'application/json',
                    'Accept':'application/json',
                },
                body:JSON.stringify({
                    name:not.name,
                    text:not.text
                })
           }).then(r=>r.json())
    }
    getAllNotes(){
        return fetch('/show',{
            method:'GET',
            headers:{
               'Content-Type':'application/json',
               'Accept':'application/json'
              },
        }).then(r=>r.json())
    }
}
