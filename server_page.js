
module.exports = (app,express)=>{
    let staticControl = new StaticController();
    app.get('/',staticControl.index)
    app.use('assets',express.static(__dirname+"/js"))
}

class StaticController{
    index(req,res){
        res.setHeader('Content-Type','text/html');
        res.sendFile(__dirname+'/index.html')
    }
}

